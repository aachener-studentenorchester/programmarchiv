/* Muss leider absolut angegeben werden, wegen Wordpress/Js-Mix */
var jsonUrlKonzerte = "https://aachener-studentenorchester.gitlab.io/programmarchiv/webpage/konzerte.json";
var jsonUrlKomponisten = "https://aachener-studentenorchester.gitlab.io/programmarchiv/webpage/komponisten.json";

var zipLink = "http://www.aachener-studentenorchester.de/aufnahmenarchiv/aso_aufnahmen/";
var pdfLink = "http://www.aachener-studentenorchester.de/aufnahmenarchiv/aso_programmhefte/";

var sortPARAM = "DESC";
var stilPARAM = "KONZ";

function padZeros(number, length) {
	var str = number + "";
	while (str.length < length) 
		str = "0" + str;
	return str;
}

var $ = jQuery;

$('#tabKomponist').hide();
zeigeKonzerte(sortPARAM);
$('#tabStil').click(function (e) {
	e.preventDefault();
	
	sortPARAM = "DESC";
	if(stilPARAM === "KONZ"){
		zeigeKomponist(sortPARAM);
		$('#tabKonzert').hide();
		$('#tabKomponist').show();
		$('#tabStil').html("Musikarchiv nach Konzert sortieren");
		stilPARAM = "KOMP";
	}
	else{
		zeigeKonzerte(sortPARAM);
		$('#tabKomponist').hide();
		$('#tabKonzert').show();
		$('#tabStil').html("Musikarchiv nach Komponist sortieren");
		stilPARAM = "KONZ";
	}
});


$('#sortKonzert').click(function (e) {
	e.preventDefault();
	
	if(sortPARAM === "ASC"){
		zeigeKonzerte("DESC");
		sortPARAM = "DESC";
	}
	else{
		zeigeKonzerte("ASC");
		sortPARAM = "ASC";
	}
});

$('#sortKomponist').click(function (e) {
	e.preventDefault();
	
	if(sortPARAM === "ASC"){
		zeigeKomponist("DESC");
		sortPARAM = "DESC";
	}
	else{
		zeigeKomponist("ASC");
		sortPARAM = "ASC";
	}
});

$(document).on("click", ".konzLink", function(){
	sortPARAM = "DESC";
	zeigeKonzerte(sortPARAM);
	$('#tabKomponist').hide();
	$('#tabKonzert').show();
	$('#tabStil').html("Musikarchiv nach Komponist sortieren");
	stilPARAM = "KONZ";
	
	$('html,body').animate({scrollTop: $("#"+this.id).offset().top},'slow');
});

function zeigeKonzerte(sortP){
	$.getJSON(jsonUrlKonzerte, function (konzData) {
		var tr;
		var td;
		
		$('#konzert').html("");
		
		if(sortP === "DESC"){
			konzData.sort(function(a,b) { 
				return b.KonzertNr - a.KonzertNr;
				
			});
		}
		
		$.getJSON(jsonUrlKomponisten, function (kompData) {
			$.each(konzData, function(k, konzert) {
				var konzertFirstTr = $('<tr/>');
				
				var anzahlStuecke = konzert.Werke.length;
				if(konzert.Zusatztitel === null){
					konzertFirstTr.append('<td rowspan=' + anzahlStuecke + ' id="konzert' + konzert.KonzertNr + '"><b>' + konzert.Semester + '</b></td>');
				}
				else{
					konzertFirstTr.append('<td rowspan=' + anzahlStuecke + ' id="konzert' + konzert.KonzertNr + '"><b>' + konzert.Semester + '</b><br>' + konzert.Zusatztitel + '</td>');
				}
				
				$('#konzert').append(konzertFirstTr);
				
				var currentTr = konzertFirstTr;
				$.each(konzert.Werke, function(w, werk) {
					var werktitel = "n/a";
					var results = $.grep(kompData, function (el1, idx) {
						res = $.grep(el1.Werke, function (el2, idx2) {
							return el2.WKey == werk.WKey;
						});
						if(res.length != 0){
							werktitel = res[0].Titel;
						}
						return (res.length != 0);
					});

					var konzertTd = $("<td/>");
					konzertTd.css('text-align', 'left');				
					
					if(results.length>0){
						if(results[0].Nachname === null){
							if(werk.Solist === null){
								konzertTd.append(werktitel + "<br>");
							}
							else{
								konzertTd.append(werktitel + "<br><span style=\"font-style: italic\">" + werk.Solist + "</span><br>");
							}
						}
						else{
							if(werk.Solist === null){
								konzertTd.append(results[0].Vorname + " " +  results[0].Nachname +  ": " + werktitel + "<br>");
							}
							else{
								konzertTd.append(results[0].Vorname + " " +  results[0].Nachname +  ": " + werktitel + "<br><span style=\"font-style: italic\">" + werk.Solist + "</span><br>");
							}
						}
					}
					
					currentTr.append(konzertTd);
					$('#konzert').append(currentTr);	
					currentTr = $('<tr/>');
				});
				
				var cdNrTd = $('<td rowspan=' + anzahlStuecke + '/>');
				if(konzert.CDNr !== null){
					cdNrTd.text(konzert.CDNr);
				}
				konzertFirstTr.append(cdNrTd);
				
				var downloadTd = $('<td rowspan=' + anzahlStuecke + '/>');
				if(konzert.CD) {
					var zipUrl = zipLink + "ASO_CD" + padZeros(konzert.CDNr, 2) + "_" + konzert.Kuerzel + ".zip";
					downloadTd.append('<a class="icon" href="' + zipUrl + '"><i class="fa fa-file-audio-o fa-lg"></i></a>');
				}
				if(konzert.Heft) {
					var pdfUrl = pdfLink + "Programmheft_" + padZeros(konzert.CDNr, 2) + "_" + konzert.Kuerzel + ".pdf";
					downloadTd.append('<a class="icon" href="' + pdfUrl + '"><i class="fa fa-file-pdf-o fa-lg"></i>');
				}
				konzertFirstTr.append(downloadTd);

			});
		});
	});
}


function zeigeKomponist(sortP){
	$.getJSON(jsonUrlKomponisten, function (kompData) {
		$('#komponist').html("");
		
		kompData.sort(function(a,b) {
			if(sortP === "DESC"){
				if ( a.Nachname < b.Nachname )
					return -1;
				if ( a.Nachname > b.Nachname )
					return 1;
				return 0;
			}
			else{
				if ( a.Nachname > b.Nachname )
					return -1;
				if ( a.Nachname < b.Nachname )
					return 1;
				return 0;
			}
		});
		
		$.getJSON(jsonUrlKonzerte, function (konzData) {
			$.each(kompData, function(k, komponist) {
				
				if(komponist.Nachname !== null){
					var komponistFirstTr = $('<tr/>');
				
					var anzahlWerke = komponist.Werke.length;
					if(komponist.Tod === null){
						komponistFirstTr.append('<td rowspan=' + anzahlWerke + '><b>' + komponist.Vorname + ' ' + komponist.Nachname +'</b><br>(*' +komponist.Geburt + ')</td>');
					} else{
						komponistFirstTr.append('<td rowspan=' + anzahlWerke + '><b>' + komponist.Vorname + ' ' + komponist.Nachname +'</b><br>(' +komponist.Geburt + ' - ' + komponist.Tod + ')</td>');
					}
					
					komponist.Werke.sort(function(a,b) {
						if ( a.Titel < b.Titel )
							return -1;
						if ( a.Titel > b.Titel )
							return 1;
						return 0;
					});
					
					var currentTr = komponistFirstTr;
					$.each(komponist.Werke, function(w, werk) {
						var werkTd = $('<td/>');
						werkTd.css('text-align', 'left');
						werkTd.text(werk.Titel);
						//td.css('border', 'none');
						currentTr.append(werkTd);
						
						var results = $.grep(konzData, function (el1, idx) {
							res = $.grep(el1.Werke, function (el2, idx2) {
								return el2.WKey == werk.WKey;
							});
							return (res.length != 0);
						});
						
						var konzertTd = $('<td/>');
						for(i=0; i<results.length; i++){
							konzertTd.append('<a class="konzLink" href="#" id="konzert' + results[i].KonzertNr + '">' + results[i].Kuerzel + '</a>');
							if (i<results.length-1) {
								konzertTd.append(', ');
							}
						}
						
						currentTr.append(konzertTd);
						$('#komponist').append(currentTr);
						
						currentTr = $('<tr/>');
					});
				}
			});
		});
	});
} 
